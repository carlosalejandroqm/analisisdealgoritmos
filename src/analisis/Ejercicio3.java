//3. Dado un array de enteros de n posiciones y un valor x determinar cuántas veces aparece x en el array.
package analisis;

/**
 *
 * @author Carlos Alejandro Quiñones Martinez.
 */
import java.util.*;

public class Ejercicio3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int x = sc.nextInt();

        int v[] = new int[n];

        for (int i = 0; i < v.length; i++) {
            v[i] = sc.nextInt();
        }

        int total = repite(v, x);
        System.out.println(total);
    }

    public static int repite(int v[], int x) {
        int cont = 0; // -----------------------> 1
        int i = 0; // -----------------------> 1

        while (i < v.length) { // -----------------------> 1
            if (v[i] == x) { // -----------------------> 2
                cont++; // -----------------------> 2
            }
            i++; // -----------------------> 2
                // -----------------------> // cuando vuelve a preguntar si i es menor a v.length --> 1
        }

        return cont; // -----------------------> 1
    }

}
// T(n) = 1 + 1 + 1 + n ( 2 + 2 + 2 + 1 ) + 1 ; 
// T(n) = 4 + 7n ---> O(n) = n 
