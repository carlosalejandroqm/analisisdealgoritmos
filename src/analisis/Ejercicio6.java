package analisis;

/**
 *
 * @author SIDSMOVIL
 */
import java.util.*;

public class Ejercicio6 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int matrix[][] = new int[n][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        simetrica(matrix); 
    }

    public static void simetrica(int m[][]) {
        int m2[][] = m;

        for (int i = 0; i < m2.length; i++) {
            
            for (int j = 0; j < m2[i].length; j++) {
                m2[i][j] = m2[j][i];
            }
           
        }
        
        // mostrar
         for (int i = 0; i < m2.length; i++) {
            for (int j = 0; j < m2[i].length; j++) {
                System.out.print(m2[i][j]+" ");
            }
            System.out.println(" ");
        }

//        return false;
    }

}
// 3 1 2 3 4 5 6 7 8 9 