//  5. Dado un array bidimensional de nxn posiciones de enteros calcular la suma de todos sus elementos.
package analisis;

/**
 *
 * @author Carlos Alejandro Quiñones Martinez
 */
import java.util.*;

public class Ejercicio5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int matrix[][] = new int[n][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println(" ");
        }

        int total = sumar(matrix);
        System.out.println(total);

    }

    public static int sumar(int m[][]) {
        int suma = 0, i = 0; // -----------------------> 2
        int j;

        while (i < m.length) { // -----------------------> 1 
            j = 0; // -----------------------> 1 
            while (j < m[i].length) { // -----------------------> 1 
                suma += m[i][j]; // -----------------------> 4
                j++; // -----------------------> 2
                // -----------------------> // cuando vuelve a preguntar si j es menor a m[i].length --> 1
            }
            i++; 
            // -----------------------> // cuando vuelve a preguntar si i es menor a m.length --> 1
        }

        return suma; // ----------------------------------------> 1 
    }

}

// T(n) = 1 + 1 + 1 + n ( 1 + 1 + n ( 4 + 2 + 1 ) + 2 + 1 ) --> 3 + n ( 5 + 7n ) 
//T(n) = 3 + 5n + 7n2 --> O(n) = n2 
