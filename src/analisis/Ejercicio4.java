//4. Dado un array de enteros de n posiciones, calcular la suma de los elementos en las posiciones pares.
package analisis;

/**
 *
 * @author Carlos Alejandro Quiñones Martinez.
 */
import java.util.*;

public class Ejercicio4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int v[] = new int[n];

        for (int i = 0; i < v.length; i++) {
            v[i] = sc.nextInt();
        }

        int total = suma(v);
        System.out.println(total);

    }

    public static int suma(int v[]) {
        int suma = 0; // -----------------------> 1

        int i = 0; // -----------------------> 1

        while (i < v.length) { // -----------------------> 1
            suma += v[i]; // -----------------------> 3
            i += 2; // -----------------------> 2
            // -----------------------> // cuando vuelve a preguntar si i es menor a v.length --> 1

        }

        return suma; // -----------------------> 1
    }

}
