package analisis;

import java.util.*;

public class Ejercicio1Potencias {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int n = sc.nextInt();
       
        int aux = calcular(x, n);
        int aux2 = calcular2(x, n);
       
        System.out.print(aux+"\n");
        System.out.print(aux2+"\n");
    }

    public static int calcular(int x, int n) {
        int res = 1 ;
         
        while(n -- > 0 ){
            res *= x ;
        }
        return res ;
    }

    public static int calcular2(int x, int n) {
        if (n == 0) {
            return 1;
        }
        return x * calcular2(x, n - 1);

    }

}